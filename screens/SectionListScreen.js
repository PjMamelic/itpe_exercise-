import React, { Component } from 'react';
import { SectionList, Text } from 'react-native';

export default class SectionListScreen extends Component {
    render() {
        return (
            <SectionList
                renderItem={({ item, index, section }) => <Text key={index}>{item}</Text>}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={{fontFamily:'calibri' , fontWeight: 'bold' }}>{title}</Text>
                )}
                sections={[
                    { title: 'TITLE1', data: ['item1', 'item2'] },
                    { title: 'TITLE2', data: ['item3', 'item4'] },
                    { title: 'TITLE3', data: ['item5', 'item6'] },
                ]}
                keyExtractor={(item, index) => item + index}
            />
        )
    }
}