import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default class DetailsScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text></Text>
        <Text>A</Text>
        <Text>BC</Text>
        <Text>DEF</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
