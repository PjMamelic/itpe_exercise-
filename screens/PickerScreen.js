import React, { Component } from 'react';
import { Text, View, StyleSheet, Picker } from 'react-native';

export default class PickerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      state: 'Java'
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.paragraph}>
          pick one
        </Text>
        <Picker
          style={{ width: 150 }}
          selectedValue={this.state.language}
          onValueChange={(lang) => this.setState({ language: lang })}>
          <Picker.Item label="Java" value="java" />
          <Picker.Item label="JavaScript" value="js" />
        </Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});
