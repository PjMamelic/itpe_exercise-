import React, { Component } from 'react';
import { View, Text } from 'react-native'; 

export default class ViewScreen extends Component {
    render() {
      return (
        <View
          style={{
            flexDirection: 'column',
            height: 100,
            padding: 20,
          }}>
          <View style={{backgroundColor: 'blue', flex: 0.5}} />
          <View style={{backgroundColor: 'red', flex: 0.5}} />
          <Text style={{alignItems:'center', color:'black'}}>yey!</Text>
        </View>
      );
    }
  }