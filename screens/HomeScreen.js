import React, { Component } from 'react';
import { StyleSheet, View, Button, ScrollView , Text } from 'react-native';

export default class HomeScreen extends Component {
  render() {
    return (
      <ScrollView>
        <View style={[styles.container, styles.spacing, alignItems='center'] }>
          <Button title="TEXT" onPress={() => this.props.navigation.navigate('Text')} />  
          <Text> </Text>
          <Button title="VIEW" onPress={() => this.props.navigation.navigate('View')} /> 
          <Text> </Text>
          <Button title="IMAGE" onPress={() => this.props.navigation.navigate('Image')} />
          <Text> </Text>
          <Button title="SLIDER" onPress={() => this.props.navigation.navigate('Slider')} />
          <Text> </Text>
          <Button title="PICKER" onPress={() => this.props.navigation.navigate('Picker')} />
          <Text> </Text>
          <Button title="MODAL" onPress={() => this.props.navigation.navigate('Modal')} />
          <Text> </Text>
          <Button title="SWITCH" onPress={() => this.props.navigation.navigate('Switch')} />
          <Text> </Text>
           <Button title="BUTTON" onPress={() => this.props.navigation.navigate('Button')} />
          <Text> </Text>
          <Button title ="DETAILS" onPress={() => this.props.navigation.navigate('Details')} />
          <Text> </Text>
          <Button title="WEBVIEW" onPress={() => this.props.navigation.navigate('WebView')} />
          <Text> </Text>
          <Button title="TEXT INPUT" onPress={() => this.props.navigation.navigate('TextInput')} />
          <Text> </Text>
          <Button title="STATUS BAR" onPress={() => this.props.navigation.navigate('StatusBar')} />
          <Text> </Text>
          <Button title="SCROLL VIEW" onPress={() => this.props.navigation.navigate('ScrollView')} />
          <Text> </Text>
          <Button title="SECTION LIST" onPress={() => this.props.navigation.navigate('SectionList')} />
          <Text> </Text>
          <Button title="PROGRESS BAR" onPress={() => this.props.navigation.navigate('PBA')} />
          <Text> </Text>
          <Button title="REFRESH CONTROL" onPress={() => this.props.navigation.navigate('RefreshControl')} />
          <Text> </Text>
          <Button title="ACTIVITY INDICATOR" onPress={() => this.props.navigation.navigate('ActivityIndicator')} />         
          <Text> </Text>
          <Button title="TOUCHABLE OPACITY" onPress={() => this.props.navigation.navigate('TO')} />
          <Text> </Text>
          <Button title="TOUCHABLE HIGHLIGHT" onPress={() => this.props.navigation.navigate('TH')} />
          <Text> </Text>
          <Button title="KEYBOARD AVOIDING VIEW" onPress={() => this.props.navigation.navigate('KAV')} />
          <Text> </Text>
          <Button title="Go to VIEW PAGER ANDROID" onPress={() => this.props.navigation.navigate('VPA')} />
          <Text> </Text>
          <Button title="TOUCHABLE NATIVE FEEDBACK" onPress={() => this.props.navigation.navigate('TNF')} />
          <Text> </Text>
          <Button title="TOUCHABLE WITHOUT FEEDBACK" onPress={() => this.props.navigation.navigate('TWF')} />
          <Text> </Text>

       </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  spacing: {
    padding: 20
  },

});
