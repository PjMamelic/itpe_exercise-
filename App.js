import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from './screens/HomeScreen';
import TextScreen from './screens/TextScreen';
import ViewScreen from './screens/ViewScreen';
import ImageScreen from './screens/ImageScreen';
import SliderScreen from './screens/SliderScreen';
import PickerScreen from './screens/PickerScreen';
import ModalScreen from './screens/ModalScreen';
import SwitchScreen from './screens/SwitchScreen';
import ButtonScreen from './screens/ButtonScreen';
import DetailsScreen from './screens/DetailsScreen';
import WebViewScreen from './screens/WebViewScreen';
import TextInputScreen from './screens/TextInputScreen';
import StatusBarScreen from './screens/StatusBarScreen';
import ScrollViewScreen from './screens/ScrollViewScreen';
import SectionListScreen from './screens/SectionListScreen';
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen';
import RefreshControlScreen from './screens/RefreshControlScreen';
import TouchableOpacityScreen from './screens/TouchableOpacityScreen';
import TouchableHighlightScreen from './screens/TouchableHighlightScreen';
import KeyboardAvoidingViewScreen from './screens/KeyboardAvoidingViewScreen';
import ViewPagerAndroidScreen from './screens/ViewPagerAndroidScreen';
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen';
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen';


const RootStack = createStackNavigator(
 
  {
    Home: {
      screen: HomeScreen,
    },
    Text: {
      screen: TextScreen,
    },
    View: {
      screen: ViewScreen,
    },
    Image: {
      screen: ImageScreen,
    },
    Slider: {
      screen: SliderScreen,
    },
    Picker: {
      screen: PickerScreen,
    }, 
    Modal: {
      screen: ModalScreen,
    },
    Switch: {
      screen: SwitchScreen,
    },
    Button: {
      screen: ButtonScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    WebView: {
      screen: WebViewScreen,
    },
    TextInput: {
      screen: TextInputScreen,
    },
    StatusBar: {
      screen: StatusBarScreen,
    },
    ScrollView: {
      screen: ScrollViewScreen,
    },
    SectionList: {
      screen: SectionListScreen,
    },
    PBA: {
      screen: ProgressBarAndroidScreen,
    },
    RefreshControl: {
      screen: RefreshControlScreen,
    },
    TO: {
      screen: TouchableOpacityScreen,
    },
     TH: {
      screen: TouchableHighlightScreen,
    },
    KAV: {
      screen: KeyboardAvoidingViewScreen,
    },
    VPA: {
      screen: ViewPagerAndroidScreen,
    },
    TNF: {
      screen: TouchableNativeFeedbackScreen,
    },
    ActivityIndicator: {
      screen: ActivityIndicatorScreen,
    },
    

  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends Component {
  render() {
    return <RootStack />
  }
}